/*
 * Public API Surface of bb-itag-angular-change-log
 */

export * from './lib/bb-itag-angular-change-log.component';
export * from './lib/bb-itag-angular-change-log.module';
export * from './lib/change-log-fix/change-log-fix.component';
export {IChangeLogInfo, IChangeLogFixInfo } from './lib/models';
