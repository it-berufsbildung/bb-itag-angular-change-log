export interface IChangeLogFixInfo {
  issueNo: number;
  title: string;
  description: string;
  type: 'bug' | 'feature';
}
