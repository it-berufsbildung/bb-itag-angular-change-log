import { IChangeLogFixInfo } from './IChangeLogFixInfo';

export interface IChangeLogInfo {
  fixes: IChangeLogFixInfo[];
  releaseDate: string; // yyyy-mm-dd
  version: string;
}
